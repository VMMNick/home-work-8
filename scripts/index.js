const paragraphs = document.querySelectorAll('p');
paragraphs.forEach(paragraph => {
  paragraph.style.backgroundColor = '#ff0000';
});

const optionsList = document.getElementById('optionsList');
console.log(optionsList);

const parentElement = optionsList.parentElement;
console.log(parentElement);

if (optionsList.childNodes.length > 0) {
  optionsList.childNodes.forEach(childNode => {
    console.log(`Назва: ${childNode.nodeName}, Тип: ${childNode.nodeType}`);
  });
};

const testParagraph = document.querySelectorAll('.testParagraph');
testParagraph.textContent = 'This is a paragraph';

const mainHeader = document.querySelector('.main-header');
const anchorElements = mainHeader.querySelectorAll('a');
anchorElements.forEach((element, index) => {
  element.classList.add('nav-item');
  console.log(`Елемент ${index + 1}:`, element);
});

const sectionTitles = document.querySelectorAll('.section-title');
sectionTitles.forEach(sectionTitle => {
  sectionTitle.classList.remove('section-title');
});
